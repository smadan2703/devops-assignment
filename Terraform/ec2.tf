provider "aws" {
	region = "${var.aws_region}"
}

// Get the Current Public of the Machine to enable SSH access
data "http" "current_ip" {
  url = "${var.http_url}"
}

// Get the latest Red Hat AMI ID
data "aws_ami" "redhat" {
  most_recent = true

  filter {
    name   = "name"
    values = ["RHEL-8.0.0_HVM-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["309956199498"] # Canonical
}

// Create Key Pair to Login Linux Ec2 Instance
resource "aws_key_pair" "ec2_web_key" {
  key_name   = "ec2_web_key"
  public_key = "${var.sshkey}"
}

// Create Security Group for the Instance
resource "aws_security_group" "sg_ec2" {
  name = "EC2 SG"
  description = "Web Security Group for Ec2"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["${chomp(data.http.current_ip.body)}/32"]
  }    
 
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }  

   ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  } 

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

// Create Ec2 Instance 
resource "aws_instance" "web" {
  ami           = "${data.aws_ami.redhat.id}"
  availability_zone = "${var.aws_region}a"
  instance_type = "${var.instance_size}"
  vpc_security_group_ids = ["${aws_security_group.sg_ec2.id}"]
  key_name = "${aws_key_pair.ec2_web_key.key_name}"
  root_block_device  {
        volume_type = "gp2"
        volume_size = "${var.root_device}"
        delete_on_termination = "true"
        encrypted = "true"
    }
  ebs_block_device {
        device_name = "/dev/xvdb"
        volume_size = "${var.block_device}"
        volume_type = "gp2"
        delete_on_termination = false
        encrypted = "true"
}
  tags = {
    Name = "Web Application"
  }
  depends_on = [aws_key_pair.ec2_web_key, aws_security_group.sg_ec2]
}


output "ec2_dns" {
  value = aws_instance.web.public_dns
}