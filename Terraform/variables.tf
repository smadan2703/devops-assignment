// Terraform Will use the Default VPC ID in the Singapore Region and Credentials are configured in Machine.

variable "aws_region" {
	default = "ap-southeast-1"
}

variable "instance_size" {
	default = "c5n.xlarge"
}

variable "http_url" {
  default = "http://ipv4.icanhazip.com"
}

variable "block_device" {
	default = "100"
}

variable "root_device" {
	default = "20"
}

variable "sshkey" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDXTqzpGlspvt1i0Ak7GUTsvtNFQlQc5w2EpUYH7VWZRieB6enbDT6ccTYpR0Nq20fmTY7kuPaPZX43X3AA8q3GXaTk86sr2NBxomaExu+Xgpz3VXH1ubI/7qli5OF1kdC9gfpDhCql7JJ7XjF10WskZ/lilkVsGtVDW/leyK4YRK8gzTjAH14uQLVJKKSjgGsbT0hTRHNLHQUg4MJPox1XPlATiXa9y7JZ5O9Xjc2UEZJliB/QoSHIK28WXnC7VmiqrQepoqv/MGIryv1fIQqV4dQFdJv8YCgOOiXeg1kkiz1MVkcLXDHWgkHUy705V1jbHF99VCF3y2ljXT6fgAqx"
}
