## Terraform Script to Launch Gigi AWS Infra

## Steps to Run Terraform Scripts

***Step 1***: Install [Terraform](https://www.terraform.io/downloads.html) in the Local or Shared Machine

***Step 2***: Unzip and copy the terraform binary to /usr/local/bin folder

***Step 3***: Verify by issuing terraform command 

***step 4***: cd <path to the folder>/Terraform/  and  issue ```terraform init``` 

* terraform init command is to download the Terraform modules 

***Step 5***:  ```terraform plan```
* This command will show if there is an error in the syntax 
* This will show resources creation, modification, deletion. 
* As mentioned it will just show it will not perform any task.
    
***step 6***: ```terraform apply```
* This is the actual command, will perform all the task

Note: do not delete or modify ```terraform.tfstate``` and ```terraform.tfstate.backup```

step 7: ```terraform destroy```
* This command, is used to delete all the recource created early.