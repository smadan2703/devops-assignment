## Ansible Script to Install Java Version 11 & 13 


## Steps to Run Ansible Scripts

**Env can be defined in the Hosts file.** 

* ***To Install Java 11***

  ```ansible-playbook  -i host java.yml -e 'env=dev version=11'```

* ***To Install Java 13***   

  ```ansible-playbook  -i host java.yml -e 'env=dev version=13'```
