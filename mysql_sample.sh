#/bin/bash

# Export the below if your using MacBook 
# Password will be prompt 

# Change the db host, db user , db port below is the example

# host=xxxx-xxxx.xxxxxxxxx.ap-southeast-1.rds.amazonaws.com
# user=xxxxx
# port=3306

#export PATH=$PATH:/Applications/MySQLWorkbench.app/Contents/MacOS

host=<db host>
user=<db user>
port=<db port>


# Get all database names

databases=$(mysql -h$host -u$user -P$port -p -Bse 'show databases' | grep -Ev "^(Database|mysql|performance_schema|sys|information_schema)$")
for db in $databases
do 
    echo "$db"
done